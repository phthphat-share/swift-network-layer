# Networking and parsing data (Deprecated)

- Use `Network` (based on URLSession) instead of Alamofire 
- Use `DataParser` (based on Codable) instead of ObjectMapper

This module has been deprecated due to the release of [PNetworking](https://gitlab.com/phthphat-share/pnetworking)

## Networking (call API)
```swift
let network = Network(baseUrl: "https://sample.com")

network.request(endPoint: SampleEndPoint.logIn(username: "abc", password: "123")) { [weak self] result in
    switch result {
    case .success(let data):
        break
    case .failure(let err):
        break
    }
}
enum SampleEndPoint {
    case logIn(username: String, password: String)
}
extension SampleEndPoint: EndPoint {
    var path: String {
        switch self {
        case .logIn:
            return "/auth"
        default:
            return ""
        }
    }
    
    var httpMethod: HttpMethod {
        switch self {
        case .logIn:
            return .post
        default:
            return .get
        }
    }
    
    var parameters: [String : Any] {
        switch self {
        case .logIn(let username, let password):
            return ["username": username, "password": password]
        default:
            return [:]
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
```
It's looked verbose with one request, but it can be your codebase with the more case in a endpoint

## DataParser
Parsing data between Dictionary, Codable Object and Data

```swift

class User: Codable {
    var id: Int?
    var username: String?
}

let userData: Data
let userParser = DataParser<User>(data: userData)

var user: User = userParser.toObject()!
var userDict: [String: Any] = userParser.toDict()!
```

## Contributor
- Phat Pham: [gitlab](https://gitlab.com/phthphat), [facebook](https://www.facebook.com/phthphat)
